<!--
<style>
table {
   width:100%;
   border: 2px solid gray;
}
</style>
-->
#Lista pytań do lekarza - dla potrzeb wykazania związku przyczynowego między NOP a szczepieniem - przed szczepieniem
Przeredagowane do potrzeb pisma prawnego sugestie w zakresie algorytmu oceny związku przyczynowo-skutkowego pomiędzy szczepieniem a [NOP w praktyce](http://www.mp.pl/szczepienia/artykuly/wytyczne/show.html?id=97606).
Przeredagowana lista pytań StopNOP w związku z petycją "Skoro jest ryzyko - musi być wybór" popartą przez ponad 20 000 osób. [Źródło.](https://www.facebook.com/stowarzyszeniestopnop/posts/1299417503419008). [Artykuł dra Jaśkowskiego](http://www.polishclub.org/2016/07/20/dr-jerzy-jaskowski-7/). Inne źródła w tym rozporządzenia MZ i ustawy.

<!-- przeredagowane do potrzeb pisma prawnego sugestie w zakresie algorytmu oceny związku przyczynowo-skutkowego pomiędzy szczepieniem a NOP w praktyce - http://www.mp.pl/szczepienia/artykuly/wytyczne/show.html?id=97606 -->

<table><tr><td>
Czy świadczeniodawca oświadcza, że przed realizacją szczepienia zostały wykonane wszystkie niezbędne czynności umożliwiające zastosowanie protokołu WHO w przypadku wystąpienia NOP w zakresie algorytmu oceny związku przyczynowo-skutkowego pomiędzy szczepieniem a zdarzeniem niepożądanym?
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Proszę wypisać pełną listę szczepionek, które będą podane - konkretnych preparatów handlowych z numerem serii.
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Oświadczam, że przed podaniem sprawdziłem online i telefonicznie wszystkie serie szczepionek czy nie zostały wycofane (m.in. GIF).
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Proszę o określenie wszystkich niekorzystnych/nieoczekiwanych objawów, wyników badań laboratoryjnych lub chorób, o których świadczeniodawca wie teraz, i które będą mogły mieć związek przyczynowy z ewentualnym wystąpieniem NOP po szczepieniu.
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Czy potwierdzam, że znane mi są prawidłowe definicje przypadków razie wystąpienia NOP po szczepieniu (np. definicja - kryteria - Brighton Collaboration, standardowa definicja podawana w piśmiennictwie, definicja zgodna z krajowymi lub innymi wytycznymi).
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Czy dysponują wiarygodnymi danymi, badaniami przedmiotowymi lub badaniami laboratoryjnymi, które mogą potwierdzić inną przyczynę zdarzenia w przypadku wystąpienia NOP? 
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Czy dysponuję badaniami określającymi bazowe wartości określonych mierzalnych wskażników, które mogą ulec po wystąpienu NOP i wskazywać na przyczynę NOP jako szczepienie? 
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Czy przeprowadziłem swoisty test, potwierdzający, że ewentualne efekty NOP nie występują już przed podaniem szczepionki? Jakie to były testy? 
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Czy oświadczam, że nie popełniłem błędu przy przepisywaniu szczepionki, postąpiłem zgodnie z zaleceniami dotyczącymi jej stosowania (np. podaję ją w zgodzie z okresem ważności, podaję ją właściwej osobie, sprawdziłem i wykluczyłem wszelkie przeciwskazania, ustaliem jałowość preparatu i narzędzi itd)? 
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Czy oświadczam, że ze szczepionką obchodzono się prawidłowo (przestrzegano zasad łańcucha chłodniczego podczas transportu lub przechowywania, nie naruszono procedury szczepienia itp.))? Proszę o dołączenie uwierzytelnionej kopii dokumentacji udowadniającej zachowanie zasad łańcucha chłodniczego w stosunku do mających być zastosowanych konkretnych preparatów szczepionkowych. Bez dokumentacji wykazującej i gwarantującej zachowanie łańcucha chodniczego od momentu wyprodukowania preparatu do czasu jego podania, nie jest możliwe przeprowadzenie szczepienia. Brak takiej dokumentacji powinien rodzić odpowiedzialność karną i zawodową.
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Proszę o podanie okresu, który będzie mógł być uznany za "okres zwiększonego ryzyka" po podaniu szczepionki.  
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Czy oświadczam, że nie są mi znane u dziecka objawy innych chorób lub zmiany w stanie zdrowia, które mogą mieć wpływ na wystąpienie NOP po podaniu szczepionki?  
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Czy oświadczam, że są mi znane wszelkie zdarzenia, które wystąpiły po podaniu poprzednich dawek innych szczepionek? Proszę o podanie tych zdarzeń.  
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Czy oświadczam, że u dziecka przed podaniem szczepionki nie doszło do ekspozycji na potencjalny czynnik ryzyka lub toksyny?
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Czy oświadczam, że u dziecka przed podaniem szczepionki nie występuje choroba o ostrym przebiegu?  
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Czy oświadczam, że są mi znane w historii dziecka zdarzenia z przeszłości, niezależne od szczepienia, które mogą być uznane po tym szczepieniu za analogiczne? Proszę wypisać te zdarzenia.
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Czy oświadczam, że są mi znane wszystkie leki, które dziecko przyjmowało przed szczepieniem? Proszę wypisać te leki.  
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Czy oświadczam, że są mi znane wszystkie składniki pożywienia i suplementy diety wpływające na biodostępność i działanie zastosowanego preparatu? Proszę wypisać te składniki pożywienia i suplementy diety.
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Czy oświadczam, że wykluczam możliwość wystąpienia u dziecka polipragmazji po szczepieniu?  
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Czy oświadczam, że są mi znane wszystkie procedury medyczne, które były stosowane wobec dziecka przed szczepieniem? Proszę wypisać te procedury medyczne.  
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Czy oświadczam, że dysponuję jaowymi warunkami do podania szczepionki (lub jej składników)?  
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Czy oświadczam, że przed zabiegiem szczepionka wygląda prawidłowo (kolor, mętność, obecność obcych substancji, itp.)? 
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<!--Przeredagowana lista pytań StopNOP w związku z petycją "Skoro jest ryzyko - musi być wybór" popartą przez ponad 20 000 osób. Źródło: https://www.facebook.com/stowarzyszeniestopnop/posts/1299417503419008-->

<table><tr><td>
Jakie badania diagnostyczne są wykonywane u dziecka {{imie_nazwisko}} przed szczepieniem BCG ordynowanym w pierwszych godzinach życia  (nawet 2-3 godziny po porodzie), w celu ustalenia zaburzeń odporności (są bezwzględnym przeciwwskazaniem do szczepienia BCG)?
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Jakie badania diagnostyczne są wykonywane u dziecka {{imie_nazwisko}} przed szczepieniem BCG ordynowanym w pierwszych godzinach życia (nawet 2-3 godziny po porodzie), w celu ustalenia ciężkich wad rozwojowych i wszelkich chorób zaburzających stan kliniczny noworodka (są bezwzględnym przeciwwskazaniem do szczepienia BCG)?
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Jakie badania diagnostyczne są wykonywane u dziecka {{imie_nazwisko}} przed szczepieniem BCG ordynowanym w pierwszych godzinach życia (nawet 2-3 godziny po porodzie), w celu całkowitego wykluczenia możliwości wystąpienia poniżej wymienionych powikłań, niejednokrotnie znacznie odłożonych w czasie:<br>
- bezdech<br>
- powiększenie węzłów chłonnych,<br>
- zropienie węzłów chłonnych<br>
- martwica węzłów chłonnych<br>
- rumień guzowaty (choroba zapalna tkanki tłuszczowej)<br>
- keloid<br>
- toczeń<br>
- uogólnione zakażenie gruźlicą BCG (np. gruźlica kości i stawów, gruźlica mózgu, zmiany w innych narządach i tkankach)<br>
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Jakie badania diagnostyczne są wykonywane u dziecka {{imie_nazwisko}} przed szczepieniem wzw B ordynowanym w pierwszych godzinach życia (nawet 2-3 godziny po porodzie), w celu całkowitego wykluczenia nadwrażliwości na substancję czynną lub którąkolwiek substancję pomocniczą, oraz całkowitego wykluczenia możliwości wystąpienia u dziecka powikłań związanych z tą szczepionką:<br>
- Zaburzenia ogólne i stany w miejscu podania: ból w miejscu podania, krwiak, zaczerwienienie, stwardnienie, obrzęk w miejscu podania, podwyższenie temperatury ciała powyżej 38,8°C, osłabienie<br>
- Zaburzenia żołądka i jelit: ból brzucha, zmniejszone łaknienie, biegunka, wymioty, kolka jelitowa<br>
- Zaburzenia wątroby i dróg żółciowych: żółtaczka noworodków, wzrost aktywności aminotransferaz<br>
- Zaburzenia układu nerwowego: bóle i zawroty głowy<br>
- Zaburzenia psychiczne: rozdrażnienie, bezsenność, ospałość, złe samopoczucie<br>
- Zaburzenia mięśniowo-szkieletowe, tkanki łącznej: ból mięśni, ból stawów<br>
- Zaburzenia układu immunologicznego: np. grzybica jamy ustnej<br>
- Zaburzenia skóry i tkanki podskórnej: wysypka, wysypka plamista, łupież różowaty<br>
- Bezdech<br>
- Zespół Guillain-Barré<br>
- toczeń rumieniowaty układowy<br>
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Jakie badania diagnostyczne są wykonywane u dziecka {{imie_nazwisko}} przed pozostałymi szczepieniami ordynowanym w pierwszych tygodniach/miesiącach życia, w celu całkowitego wykluczenia możliwości wystąpienia powikłań poszczepiennych ujętych w rozporządzeniu Ministra Zdrowia, informacjach producenta i wynikach badań naukowych.
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<!-- źródło: http://www.polishclub.org/2016/07/20/dr-jerzy-jaskowski-7/ -->
<table><tr><td>
Czy dziecko posiada prawidłowe poziomy hematokrytu - Ht - ustalone w oparciu o aktualne badanie (morfologię)? Proszę podać zbadany poziom/dołączyć uwierzytelnioną kopię wyników badania.
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>


<table><tr><td>
Czy dziecko posiada prawidłowe poziomy żelaza - ustalone w oparciu o aktualne badanie? Proszę podać zbadany poziom/dołączyć uwierzytelnioną kopię wyników badania.
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Czy potwierdzam, że aktualny poziom żelaza u dziecka nie przekracza 60-70mg - poziom ustalony w oparciu o aktualne badanie.
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Czy potwierdzam, że aktualny poziom 25 OHD u dziecka nie przekracza 60-70ng - poziom ustalony w oparciu o aktualne badanie. Proszę podać zbadany poziom/dołączyć uwierzytelnioną kopię wyników badania.
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Czy potwierdzam, że aktualny poziom fibrynogenu u dziecka jest prawidłowy - poziom ustalony w oparciu o aktualne badanie. Proszę podać zbadany poziom/dołączyć uwierzytelnioną kopię wyników badania.
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Czy potwierdzam, że dziecko nie ma uszkodzonej wątroby - w oparciu o aktualne badanie. Proszę dołączyć uwierzytelnioną kopię wyników badania.
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Czy potwierdzam, że aktualny poziom białka CRP u dziecka jest prawidłowy - poziom ustalony w oparciu o aktualne badanie. Proszę podać zbadany poziom/dołączyć uwierzytelnioną kopię wyników badania.
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Czy potwierdzam, że dziecko nie ma stanu zapalnego tzw. wczesnego - w oparciu o aktualne badanie. Proszę dołączyć uwierzytelnioną kopię wyników badania.
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Czy potwierdzam, że aktualny poziom OB u dziecka jest prawidłowy - poziom ustalony w oparciu o aktualne badanie tradycyjną (nie wirówkową). Proszę podać zbadany poziom/dołączyć uwierzytelnioną kopię wyników badania.
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Czy potwierdzam, że dziecko nie ma stanu zapalnego tzw. długotrwałego - w oparciu o aktualne badanie. Proszę dołączyć uwierzytelnioną kopię wyników badania.
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<!-- źródło: rozporządzenie MZ w sprawie szczepień ochronnych -->

<table><tr><td>
Dołączam uwierzytelnioną kopię dokumentów potwierdzających, że osoba przeprowadzająca szczepienie ochronne odbyła w ramach doskonalenia zawodowego kurs lub szkolenie w zakresie szczepień ochronnych i uzyskała dokument potwierdzający ukończenie tego kursu lub szkolenia lub uzyskała specjalizację w dziedzinie, w przypadku której ramowy program kształcenia podyplomowego obejmował problematykę szczepień ochronnych na podstawie przepisów o zawodach lekarza i lekarza dentysty oraz przepisów o zawodach pielęgniarki i położnej.
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>

<table><tr><td>
Dołączam uwierzytelnioną kopię dokumentów potwierdzających, że osoba przeprowadzająca szczepienie ochronne posiada co najmniej 6-cio miesięczną praktykę w zakresie przeprowadzania szczepień ochronnych. Należy wpisać okres praktyki w latach i miesiącach. Nie jest wiarygodnym potwierdzeniem dokument poświadczający jedynie okres pracy na danym stanowisku.
</td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odpowiadam:<br><br><br><br><br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;odmawiam udzielenia odpowiedzi z powodu:<br><br></td></tr><tr><td>&nbsp;&nbsp;&nbsp;&nbsp;☐&nbsp;nie znam odpowiedzi na to pytanie</td></tr></table><br>
